package com.tochanenko.bsaspring.convertor;

import com.tochanenko.bsaspring.dto.HistoryDTO;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class HistoryConvertor {
    private static final Logger logger = LoggerFactory.getLogger(HistoryConvertor.class);

    public HistoryDTO convertToDTO(String[] history) {
        if (history.length < 3) {
            logger.error("History must be more then 3 in length.");
            throw new IllegalArgumentException("History must be more then 3 in length.");
        }

        return new HistoryDTO(history[0], history[1], history[2]);
    }

    public List<HistoryDTO> convertToDTOList(List<String[]> strings) {
        return strings.stream().map(this::convertToDTO).collect(Collectors.toList());
    }

}

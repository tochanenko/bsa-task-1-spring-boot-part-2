package com.tochanenko.bsaspring.convertor;

import com.tochanenko.bsaspring.dto.CacheDTO;
import com.tochanenko.bsaspring.entity.Cache;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class CacheConvertor {
    public CacheDTO convertToDTO(Cache cache) {
        return new CacheDTO(
                cache.getTag(),
                cache.getGifs().stream()
                        .map(it -> ServletUriComponentsBuilder.fromCurrentContextPath()
                                .path("/data/")
                                .path(it)
                                .toUriString()).collect(Collectors.toSet())
        );
    }

    public List<CacheDTO> convertToDTOList(List<Cache> caches) {
        return caches
                .stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }
}

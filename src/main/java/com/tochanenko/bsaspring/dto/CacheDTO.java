package com.tochanenko.bsaspring.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Set;

@Data
@AllArgsConstructor
public class CacheDTO {
    private String tag;
    private Set<String> gifs;
}

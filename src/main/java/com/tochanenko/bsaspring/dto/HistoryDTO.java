package com.tochanenko.bsaspring.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class HistoryDTO {
    private String date;
    private String tag;
    private String gif;
}

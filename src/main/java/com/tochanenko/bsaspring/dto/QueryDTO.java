package com.tochanenko.bsaspring.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class QueryDTO {
    private String tag;
    private Boolean force;
}

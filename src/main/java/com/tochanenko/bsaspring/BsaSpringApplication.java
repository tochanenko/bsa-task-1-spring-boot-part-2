package com.tochanenko.bsaspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BsaSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(BsaSpringApplication.class, args);
	}

}

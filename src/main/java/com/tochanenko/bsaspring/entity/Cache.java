package com.tochanenko.bsaspring.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Set;

@Data
@AllArgsConstructor
public class Cache {
    String tag;
    Set<String> gifs;
}

package com.tochanenko.bsaspring.controller;

import com.tochanenko.bsaspring.dto.CacheDTO;
import com.tochanenko.bsaspring.dto.QueryDTO;
import com.tochanenko.bsaspring.service.CacheControllerService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cache")
@RequiredArgsConstructor
public class CacheController {
    public static final Logger logger = LoggerFactory.getLogger(CacheController.class);

    private final CacheControllerService cacheService;

    @GetMapping
    public List<CacheDTO> getCache(@RequestParam(required = false) String query) {
        if (query == null)
            return cacheService.getEverything();
        return List.of(cacheService.findByTag(query));
    }

    @PostMapping("/generate")
    public CacheDTO generateGif(@RequestBody QueryDTO queryDTO) {
        return cacheService.generate(queryDTO.getTag());
    }

    @DeleteMapping
    public void delete() {
        cacheService.clear();
    }
}

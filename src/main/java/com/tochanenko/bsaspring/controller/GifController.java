package com.tochanenko.bsaspring.controller;

import com.tochanenko.bsaspring.service.GifControllerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class GifController {
    private final GifControllerService gifControllerService;

    @GetMapping("/gifs")
    public List<String> getEverything() {
        return gifControllerService.getEverything();
    }
}

package com.tochanenko.bsaspring.controller;

import com.tochanenko.bsaspring.dto.CacheDTO;
import com.tochanenko.bsaspring.dto.HistoryDTO;
import com.tochanenko.bsaspring.dto.QueryDTO;
import com.tochanenko.bsaspring.service.CacheControllerService;
import com.tochanenko.bsaspring.service.GifControllerService;
import com.tochanenko.bsaspring.service.UserControllerService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {
    private static final Logger logger = LoggerFactory.getLogger(UserController.class);
    private final UserControllerService userService;
    private final GifControllerService gifControllerService;
    private final CacheControllerService cacheControllerService;
    @Value("${file.users-dir}")
    private String usersDirectory;
    @Value("${file.cache-dir}")
    private String cacheDirectory;

    @PostMapping("/{id}/generate")
    public String generateGif(@PathVariable String id, @RequestBody QueryDTO queryDTO) {
        userService.validate(id);

        return ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/data/")
                .path(
                        (queryDTO.getForce() != null && queryDTO.getForce())
                                ? generateRandomGif(id, queryDTO) : findRandomGif(id, queryDTO)

                ).toUriString();
    }

    @GetMapping("/{id}/history")
    public List<HistoryDTO> getHistory(@PathVariable String id) {
        userService.validate(id);
        return userService.getHistoryForUser(id);
    }

    @GetMapping("/{id}/all")
    public List<CacheDTO> getAllTags(@PathVariable String id) {
        userService.validate(id);
        return userService.getAllTags(id);
    }

    @DeleteMapping("/{id}/history/clean")
    public void clearHistory(@PathVariable String id) {
        userService.validate(id);
        userService.clearHistory(id);
    }

    @DeleteMapping("/{id}/reset")
    public void clearCache(@PathVariable String id, @RequestParam(required = false) String tag) {
        userService.validate(id);
        if (tag == null)
            userService.clearUserCache(id);
        else
            userService.clearUserCacheByTag(id, tag);
    }

    @DeleteMapping("/{id}/clean")
    public void cleadAll(@PathVariable String id) {
        userService.validate(id);
        userService.cleanAll(id);
    }

    @GetMapping("/{id}/search")
    public String getGifForUser(
            @PathVariable String id,
            @RequestParam String query,
            @RequestParam(required = false) Boolean force
    ) {
        userService.validate(id);

        if (force != null && force) {
            return ServletUriComponentsBuilder.fromCurrentContextPath()
                    .path("/data/")
                    .path(findDiskedGif(id, query))
                    .toUriString();
        }

        return findCachedGif(id, query);
    }

    private String generateRandomGif(String id, QueryDTO queryDTO) {
        String randomGif = gifControllerService.findRandomGif(
                queryDTO.getTag(),
                usersDirectory + "/" + id
        );
        gifControllerService.copy(queryDTO.getTag(), cacheDirectory, randomGif);
        userService.saveHistory(id, queryDTO.getTag(), randomGif);
        return randomGif;
    }

    private String findRandomGif(String id, QueryDTO queryDTO) {
        String foundGif = gifControllerService.copy(
                queryDTO.getTag(),
                usersDirectory + "/" + id,
                cacheControllerService.findRandomByTag(queryDTO.getTag())
        );

        if (foundGif == null)
            return generateRandomGif(id, queryDTO);

        userService.saveHistory(id, queryDTO.getTag(), foundGif);
        return foundGif;
    }

    public String findCachedGif(String id, String tag) {
        String res = userService.findCachedGif(id, tag);

        if (res == null)
            return findDiskedGif(id, tag);

        return res;
    }

    public String findDiskedGif(String id, String tag) {
        String res = userService.findDiskedGif(id, tag);

        if (res == null) {
            logger.error("Gif not found!");
            throw new RuntimeException("Gif not found!");
        }

        return res;
    }
}

package com.tochanenko.bsaspring.service;

import com.tochanenko.bsaspring.entity.Cache;
import com.tochanenko.bsaspring.repository.GifRepository;
import com.tochanenko.bsaspring.repository.UserRepository;
import com.tochanenko.bsaspring.utils.SetRandomizer;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserService {
    private static final Logger logger = LoggerFactory.getLogger(UserService.class);

    private final UserRepository userRepository;
    private final GifRepository gifRepository;
    private final SetRandomizer setRandomizer;

    @Value("${file.users-dir}")
    private String usersDirectory;

    public void saveHistory(String id, String tag, String gif) {
        try {
            userRepository.saveHistory(
                    new Date(),
                    id,
                    tag,
                    gif
            );
        } catch (IOException e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
    }

    public void saveCache(UUID uuid, String tag, String gif) {
        userRepository.saveCache(uuid, tag, gif);
    }

    public String findCachedGif(UUID id, String tag) {
        return userRepository.findCachedGif(id, tag);
    }

    public String findDiskedGif(String id, String tag) {
        Set<String> gifs = gifRepository.getFilesInPath(usersDirectory + "/" + id + "/" + tag);
        return setRandomizer.random(gifs);
    }

    public void clearUserCache(UUID uuid) {
        userRepository.clearUserCache(uuid);
    }

    public void clearUserCacheByTag(UUID uuid, String tag) {
        userRepository.clearUserCacheByTag(uuid, tag);
    }

    public void clearHistory(String id) {
        try {
            userRepository.clearHistory(id);
        } catch (IOException e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
    }

    public List<String[]> getHistoryForUser(String id) {
        try {
            return userRepository.getHistoryForUser(id);
        } catch (IOException e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }

        return Collections.emptyList();
    }

    public List<Cache> getAllTags(String id) {
        return gifRepository.getAllTags(usersDirectory + "/" + id)
                .stream()
                .map(it -> new Cache(
                        it,
                        gifRepository.getFilesInPath(usersDirectory + "/" + id + "/" + it))
                ).collect(Collectors.toList());
    }

    public void cleanAll(String id) {
        try {
            userRepository.cleanAll(id);
        } catch (IOException e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
    }
}

package com.tochanenko.bsaspring.service;

import com.tochanenko.bsaspring.convertor.CacheConvertor;
import com.tochanenko.bsaspring.convertor.HistoryConvertor;
import com.tochanenko.bsaspring.dto.CacheDTO;
import com.tochanenko.bsaspring.dto.HistoryDTO;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserControllerService {
    private static final Logger logger = LoggerFactory.getLogger(UserControllerService.class);

    private final UserService userService;
    private final HistoryConvertor historyConvertor;
    private final CacheConvertor cacheConvertor;

    public List<CacheDTO> getAllTags(String id) {
        return cacheConvertor.convertToDTOList(userService.getAllTags(id));
    }

    public void validate(String id) {
        if (!id.matches("(?:[^\\\\/:*?\"<>|\\r\\n]+)")) {
            logger.error("Invalid ID: " + id);
            System.out.println("Doesn't match");
            throw new RuntimeException("Invalid ID: " + id);
        }
    }

    public void saveHistory(String id, String tag, String gif) {
        userService.saveHistory(id, tag, gif);
        userService.saveCache(UUID.nameUUIDFromBytes(id.getBytes()), tag, gif);
    }

    public void clearUserCache(String id) {
        userService.clearUserCache(UUID.nameUUIDFromBytes(id.getBytes()));
    }

    public void clearUserCacheByTag(String id, String tag) {
        userService.clearUserCacheByTag(UUID.nameUUIDFromBytes(id.getBytes()), tag);
    }

    public void clearHistory(String id) {
        userService.clearHistory(id);
    }

    public List<HistoryDTO> getHistoryForUser(String id) {
        return historyConvertor.convertToDTOList(userService.getHistoryForUser(id));
    }

    public void cleanAll(String id) {
        clearUserCache(id);
        userService.cleanAll(id);
    }

    public String findCachedGif(String id, String tag) {
        return userService.findCachedGif(UUID.nameUUIDFromBytes(id.getBytes()), tag);
    }

    public String findDiskedGif(String id, String tag) {
        String res = userService.findDiskedGif(id, tag);

        if (res != null)
            userService.saveCache(UUID.nameUUIDFromBytes(id.getBytes()), tag, res);

        return res;
    }
}

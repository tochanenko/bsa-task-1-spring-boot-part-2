package com.tochanenko.bsaspring.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tochanenko.bsaspring.repository.CacheRepository;
import com.tochanenko.bsaspring.repository.GifRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class GifService {
    public static final Logger logger = LoggerFactory.getLogger(GifService.class);

    private final GifRepository gifRepository;
    private final CacheRepository cacheRepository;
    private final RestTemplateBuilder restTemplateBuilder;

    public String findByUrl(String path, String url) {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_OCTET_STREAM));

            HttpEntity<String> entity = new HttpEntity<>(headers);

            ResponseEntity<byte[]> responseEntity = restTemplateBuilder
                    .build()
                    .exchange(url, HttpMethod.GET, entity, byte[].class);
            return gifRepository.save(path, responseEntity.getBody());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }

        return null;
    }

    public String findRandomGif(String resourceUrl, String path) throws JsonProcessingException {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<String> entity = new HttpEntity<>(headers);

        ResponseEntity<String> response = restTemplateBuilder
                .build()
                .exchange(resourceUrl, HttpMethod.GET, entity, String.class);

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode root = objectMapper.readTree(response.getBody());
        JsonNode id = root.path("data").path("id");
        JsonNode url = root.path("data").path("images").path("original").path("url");

        return findByUrl(path + "/" + id.asText(), url.asText());
    }

    public String copy(String path, String URL) {
        return gifRepository.copy(path, URL);
    }

    public Set<String> getFilesInPath(String path) {
        return gifRepository.getFilesInPath(path);
    }

    public List<String> getEverything(String path) {
        return gifRepository
                .getAllTags(path)
                .stream()
                .flatMap(it -> getFilesInPath(path + "/" + it).stream())
                .collect(Collectors.toList());
    }
}

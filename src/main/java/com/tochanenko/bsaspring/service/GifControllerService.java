package com.tochanenko.bsaspring.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@Service
@RequiredArgsConstructor
public class GifControllerService {
    public static final Logger logger = LoggerFactory.getLogger(GifControllerService.class);

    private final GifService gifService;

    @Value("${giphy.api_key}")
    private String apiKey;

    @Value("${giphy.random}")
    private String randomUrl;

    @Value("${file.cache-dir}")
    private String cacheDirectory;

    public String findRandomGif(String tag, String path) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(randomUrl)
                .queryParam("api_key", apiKey)
                .queryParam("tag", tag);

        try {
            return gifService.findRandomGif(builder.toUriString(), path + "/" + tag);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }

        return null;
    }

    public List<String> getEverything() {
        return gifService.getEverything(cacheDirectory);
    }

    public String copy(String tag, String path, String URL) {
        return gifService.copy(path + "/" + tag, URL);
    }
}

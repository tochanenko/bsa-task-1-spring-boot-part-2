package com.tochanenko.bsaspring.service;

import com.tochanenko.bsaspring.entity.Cache;
import com.tochanenko.bsaspring.repository.CacheRepository;
import com.tochanenko.bsaspring.repository.GifRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CacheService {
    public static final Logger logger = LoggerFactory.getLogger(CacheService.class);

    private final GifRepository gifRepository;
    private final CacheRepository cacheRepository;

    @Value("${file.cache-dir}")
    private String cacheDirectory;

    public Cache findByTag(String tag) {
        return new Cache(tag, gifRepository.getFilesInPath(cacheDirectory + "/" + tag));
    }

    public void clear() {
        try {
            cacheRepository.clear();
        } catch (IOException e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
    }

    public List<Cache> getEverything() {
        return gifRepository
                .getAllTags(cacheDirectory)
                .stream()
                .map(this::findByTag)
                .collect(Collectors.toList());
    }
}

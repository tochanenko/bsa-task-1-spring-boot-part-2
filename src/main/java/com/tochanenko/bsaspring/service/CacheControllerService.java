package com.tochanenko.bsaspring.service;

import com.tochanenko.bsaspring.controller.CacheController;
import com.tochanenko.bsaspring.convertor.CacheConvertor;
import com.tochanenko.bsaspring.dto.CacheDTO;
import com.tochanenko.bsaspring.utils.SetRandomizer;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/cache")
@RequiredArgsConstructor
public class CacheControllerService {
    public static final Logger logger = LoggerFactory.getLogger(CacheController.class);

    private final GifControllerService gifControllerService;
    private final CacheService cacheService;
    private final CacheConvertor cacheConvertor;
    private final SetRandomizer setRandomizer;

    @Value("${file.cache-dir}")
    private String cacheDirectory;

    public CacheDTO generate(String tag) {
        gifControllerService.findRandomGif(tag, cacheDirectory);
        return findByTag(tag);
    }

    public List<CacheDTO> getEverything() {
        return cacheConvertor.convertToDTOList(cacheService.getEverything());
    }

    public void clear() {
        cacheService.clear();
    }

    public String findRandomByTag(String tag) {
        Set<String> gifs = cacheService.findByTag(tag).getGifs();

        return setRandomizer.random(gifs);
    }

    public CacheDTO findByTag(String tag) {
        return cacheConvertor.convertToDTO(cacheService.findByTag(tag));
    }
}

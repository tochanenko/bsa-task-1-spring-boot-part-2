package com.tochanenko.bsaspring.repository;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.tochanenko.bsaspring.utils.SetRandomizer;
import lombok.RequiredArgsConstructor;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.*;

@Repository
@RequiredArgsConstructor
public class UserRepository {

    private static final Map<UUID, Map<String, Set<String>>> userCache = new HashMap<>();
    private final SetRandomizer setRandomizer;

    @Value("${file.gifs-dir}")
    private String currentDirectory;
    @Value("${file.users-dir}")
    private String usersDirectory;
    @Value("${file.cache-dir}")
    private String cacheDirectory;
    @Value("${file.history}")
    private String history;

    public void saveHistory(Date date, String id, String tag, String gif) throws IOException {
        String formattedDate = new SimpleDateFormat("yyyy-MM-dd").format(date);

        Path path = normalizePath(usersDirectory + "/" + id + "/" + history);
        CSVWriter csvWriter = new CSVWriter(new FileWriter(
                normalizePath(usersDirectory + "/" + id + "/" + history).toString(),
                true
        ));
        csvWriter.writeNext(new String[]{formattedDate, tag, gif});
        csvWriter.close();
    }

    public void saveCache(UUID uuid, String tag, String gif) {
        if (!userCache.containsKey(uuid))
            userCache.put(uuid, new HashMap<>());

        if (!userCache.get(uuid).containsKey(tag))
            userCache.get(uuid).put(tag, new HashSet<>());

        userCache.get(uuid).get(tag).add(ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/data/")
                .path(gif)
                .toUriString());
    }

    private Path normalizePath(String path) throws IOException {
        Path filePath = Paths.get(currentDirectory + path);
        Path fileDirectory = filePath.getParent();

        if (!Files.exists(fileDirectory)) {
            Files.createDirectories(fileDirectory);
        }

        return filePath;
    }

    public void clearHistory(String id) throws IOException {
        Files.newBufferedWriter(
                normalizePath(usersDirectory + "/" + id + "/" + history),
                StandardOpenOption.TRUNCATE_EXISTING
        );
    }

    public List<String[]> getHistoryForUser(String id) throws IOException {
        Reader reader = Files.newBufferedReader(
                normalizePath(usersDirectory + "/" + id + "/" + history)
        );

        CSVReader csvReader = new CSVReader(reader);

        List<String[]> res;
        res = csvReader.readAll();
        reader.close();
        csvReader.close();
        return res;
    }

    public void clearUserCache(UUID uuid) {
        userCache.remove(uuid);
    }

    public void clearUserCacheByTag(UUID uuid, String tag) {
        userCache.get(uuid).remove(tag);
    }

    public void cleanAll(String id) throws IOException {
        File dir = new File(currentDirectory + usersDirectory + "/" + id);
        FileUtils.deleteDirectory(dir);
    }

    public String findCachedGif(UUID id, String tag) {
        if (!userCache.containsKey(id) || !userCache.get(id).containsKey(tag))
            return null;

        Set<String> gifs = userCache.get(id).get(tag);

        return setRandomizer.random(gifs);
    }

}

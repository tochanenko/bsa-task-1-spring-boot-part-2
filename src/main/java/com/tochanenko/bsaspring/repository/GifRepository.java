package com.tochanenko.bsaspring.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Repository
public class GifRepository {
    public static final Logger logger = LoggerFactory.getLogger(GifRepository.class);

    @Value("${file.gifs-dir}")
    private String currentDirectory;

    public String save(String path, byte[] file) {
        try {
            Path filePath = normalizePath(path + ".gif");
            Files.write(filePath.toAbsolutePath(), file);
        } catch (IOException e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }

        return path + ".gif";
    }

    public String copy(String path, String URL) {
        Path filePath = Path.of(currentDirectory + URL);

        try {
            Path normalized = normalizePath(path + "/" + filePath.getFileName());
            if (!Files.exists(normalized))
                Files.copy(Path.of(currentDirectory + URL), normalized.toAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }

        return path + "/" + filePath.getFileName();
    }

    private Path normalizePath(String path) throws IOException {
        Path filePath = Paths.get(currentDirectory + path);
        Path fileDirectory = filePath.getParent();

        if (!Files.exists(fileDirectory)) {
            Files.createDirectories(fileDirectory);
        }

        return filePath;
    }

    public Set<String> getFilesInPath(String path) {
        Set<String> files = new HashSet<>();

        File directory = new File(currentDirectory + path);

        // Directory is empty or it doesn't exist
        if (!directory.exists() || directory.listFiles() == null)
            return files;

        for (File file : Objects.requireNonNull(directory.listFiles())) {
            files.add(path + "/" + file.getName());
        }

        return files;
    }

    public Set<String> getAllTags(String path) {
        Set<String> tags = new HashSet<>();

        File directory = new File(currentDirectory + path);

        if (!directory.exists() || directory.listFiles() == null)
            return tags;

        for (File file : Objects.requireNonNull(directory.listFiles())) {
            if (file.isDirectory())
                tags.add(file.getName());
        }

        return tags;
    }
}

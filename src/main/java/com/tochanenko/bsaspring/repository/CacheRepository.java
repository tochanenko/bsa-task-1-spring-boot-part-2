package com.tochanenko.bsaspring.repository;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Repository
public class CacheRepository {
    public static final Logger logger = LoggerFactory.getLogger(CacheRepository.class);

    @Value("${file.gifs-dir}")
    private String gifsDirectory;

    @Value("${file.cache-dir}")
    private String cacheDir;

    public void clear() throws IOException {
        Path path = Paths.get(gifsDirectory + cacheDir);
        Path upperDirectory = path.getParent();

        FileUtils.cleanDirectory(new File(path.toString()));
    }

    public Set<String> getTags() {
        Set<String> tags = new HashSet<>();

        File directory = new File(gifsDirectory + cacheDir);

        // Directory is empty or it doesn't exist
        if (!directory.exists() || directory.listFiles() == null)
            return tags;

        for (File tag : Objects.requireNonNull(directory.listFiles())) {
            tags.add(tag.getName());
        }

        return tags;
    }
}

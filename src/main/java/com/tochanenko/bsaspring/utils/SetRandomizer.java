package com.tochanenko.bsaspring.utils;

import org.springframework.stereotype.Component;

import java.util.Random;
import java.util.Set;

@Component
public class SetRandomizer {
    public String random(Set<String> set) {
        if (set.isEmpty())
            return null;

        int randomGif = new Random().nextInt(set.size());
        int iterator = 0;

        for (String setElement : set) {
            if (iterator++ == randomGif)
                return setElement;
        }

        return null;
    }
}
